// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VRShooterBaseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VRSHOOTERBASE_API AVRShooterBaseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
