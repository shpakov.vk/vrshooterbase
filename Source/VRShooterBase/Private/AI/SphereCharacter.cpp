// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/SphereCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"

// Sets default values
ASphereCharacter::ASphereCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	

	RootComponent->DestroyComponent(); 
	Root = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	RootComponent = Root;

	GetMesh()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	GetMesh()->SetupAttachment(Root); 
	GetArrowComponent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	GetArrowComponent()->SetupAttachment(Root); 

}

// Called when the game starts or when spawned
void ASphereCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASphereCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASphereCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

