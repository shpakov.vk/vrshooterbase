// Copyright Epic Games, Inc. All Rights Reserved.

#include "VRShooterBase.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VRShooterBase, "VRShooterBase" );
